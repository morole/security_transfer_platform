set(STPSVR_SRCS
stpsvr_msg_dispatcher.cc
stp_server.cc
main.cc
stp_dao.cc
)

ADD_EXECUTABLE(stpserver ${STPSVR_SRCS})
target_link_libraries(stpserver pthread rt glog event stputil stpcomm codec)

if(NOT CMAKE_BUILD_NO_EXAMPLES)
 add_subdirectory(tests)
endif()

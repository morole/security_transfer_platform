set(STP_CRYPTO_SRCS
stp_crypto.cc
stp_msg_packer.cc
stp_crypto_access.cc
)

add_library(stpcrypto SHARED ${STP_CRYPTO_SRCS})
# target_link_libraries(stpcrypto pthread rt glog event)


file(GLOB HEADERS "*.h")
# install(FILES ${HEADERS} DESTINATION include/stpcrypto)

install(TARGETS stpcrypto DESTINATION lib)

if(NOT CMAKE_BUILD_NO_EXAMPLES)
 add_subdirectory(tests)
endif()
